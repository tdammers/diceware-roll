#!/usr/bin/env python

from random import SystemRandom
from math import log
import sys

def count_bits(x):
    return log(x) / log(2)

r = SystemRandom()

words = []

for line in open("/usr/share/dict/words", "r"):
    words.append(line.strip())

passphrase_length = 4

for j in range (0, 4):
    passphrase = []
    for i in range(0, passphrase_length):
        passphrase.append(r.choice(words))
    passphrase = ' '.join(passphrase)

    print passphrase

entropy = len(words) ** passphrase_length
entropy_bits = count_bits(entropy)
sys.stdout.write("Entropy: {0} bits\n".format(round(entropy_bits, 2)))

entropy = 70 ** 8
entropy_bits = count_bits(entropy)
sys.stdout.write("Entropy for 8 random characters out of 70: {0} bits\n".format(round(entropy_bits, 2)))
